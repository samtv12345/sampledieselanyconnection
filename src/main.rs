mod schema;

use std::env::var;
use std::pin::Pin;
use diesel::pg::PgQueryBuilder;
use diesel::query_builder::QueryBuilder;
use diesel::{RunQueryDsl, sql_query};
use diesel::sqlite::SqliteQueryBuilder;
use diesel::prelude::*;
use diesel::sql_types::Nullable;
use diesel::sql_types::*;
use crate::schema::episodes;
use chrono::NaiveDateTime;

#[derive(diesel::MultiConnection)]
pub enum AnyConnection {
    Postgresql(PgConnection),
    Sqlite(SqliteConnection),
}


#[derive(Debug,Queryable, QueryableByName,Insertable, Clone)]
pub struct Episode{
    #[diesel(sql_type = Integer)]
    pub id: i32,
    #[diesel(sql_type = Text)]
    pub username: String,
    #[diesel(sql_type = Text)]
    pub device: String,
    #[diesel(sql_type = Text)]
    pub podcast: String,
    #[diesel(sql_type = Text)]
    pub episode: String,
    #[diesel(sql_type = Timestamp)]
    pub timestamp: NaiveDateTime,
    #[diesel(sql_type = Nullable<Text>)]
    pub guid: Option<String>,
    #[diesel(sql_type = Text)]
    pub action: String,
    #[diesel(sql_type = Nullable<Integer>)]
    pub started:Option<i32>,
    #[diesel(sql_type = Nullable<Integer>)]
    pub position:Option<i32>,
    #[diesel(sql_type = Nullable<Integer>)]
    pub total:Option<i32>,
}


fn get_conn()->AnyConnection{
    AnyConnection::establish(&var("DATABASE_URL").unwrap()).unwrap()
}

fn get_correct_builder(conn: AnyConnection) -> Pin<Box<dyn QueryBuilder<AnyConnection>>> {
    return match conn {
        AnyConnection::Postgresql(_) => Box::pin(PgQueryBuilder::new()),
        AnyConnection::Sqlite(_) => Box::pin(SqliteQueryBuilder::new()),
    }
}

fn main() {
    let conn  = get_conn();
    // Can I outsource this to a single type where I can simply call MyType::new()?

    let mut builder = get_correct_builder(conn);

    let conn  = get_conn();

    builder.push_sql("SELECT * FROM (SELECT * FROM episodes,podcasts WHERE username=");
    builder.push_bind_param();
    builder.push_sql(" AND episodes.podcast=podcasts.rssfeed AND episodes.episode = ");
    builder.push_bind_param();
    builder.push_sql(" ORDER BY timestamp DESC) GROUP BY episode  LIMIT 10;");

    let query = builder.finish();
    //TODO Debug with not downloaded podcast episode
    let res = sql_query(query)
        .bind::<Text, _>("test")
        .bind::<Text,_>("test".to_string())
        .load::<Episode>(conn)
        .expect("");
    println!("Hello, world!");
}
